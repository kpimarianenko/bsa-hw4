const monthNames = [
    "January", "February", "March",
    "April", "May", "June",
    "July", "August", "September",
    "October", "November", "December"
];

export const getMonthName = (date) => {
    return monthNames[date.getMonth()];
};

export const getTimeFromDate = dateStr => {
    const date = new Date(dateStr);
    const hours = date.getHours();
    const minutes = date.getMinutes();
    return `${hours}:${minutes < 10 ? `0${minutes}` : minutes}`;
}

export const getRelativeDateString = (date) => {
    const curDate = new Date();
    const curDay = curDate.getDate();
    const curMonth = getMonthName(curDate);
    const curYear = curDate.getFullYear();
    const day = date.getDate();
    const month = getMonthName(date);
    const year = date.getFullYear();
    if (curDay === day && curMonth === month && year === curYear) {
      return 'Today';
    } else if (curDay - 1 === day && curMonth === month && year === curYear) {
      return 'Yesterday';
    } else {
      return `${day} ${month} ${year === curYear ? '' : year}`;
    }
  }