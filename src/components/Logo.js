function Logo({ appName, appLogo }) {
  return (
    <div className="header__logo">
      <h1>{ appName }</h1>
      <img src={ appLogo } alt="logo"/>
    </div>
  );
}

export default Logo;
