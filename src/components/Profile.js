function Profile({ name, avatar }) {
  return (
    <div className="profile">
      <h3>{name}</h3>
      <img src={avatar} alt={`${name}'s ava`} />
    </div>
  );
}

export default Profile;
