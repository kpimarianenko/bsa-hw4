import Message from './Message';

function MessageGroup({ day, messages, showEditModal }) {
  const mapMessages = (messages) => {
    messages.sort((a,b) => {
      return new Date(a.createdAt).getTime() < new Date(b.createdAt).getTime() ? -1 : 1
    })
    return messages.map(message => (
      <Message showEditModal={showEditModal} message={message} key={message.id} />
    ));
  }

  return (
    <div>
        <div className="message__divider">{day}</div>
        { mapMessages(messages) }
    </div>
  );
}

export default MessageGroup;