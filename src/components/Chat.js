import '../styles/Chat.css';
import ChatHeader from './ChatHeader';
import MessageList from './MessageList';
import MessageInput from './MessageInput';

function Chat({ participiants, messages, isLoaded }) {
  return (
    <div className="chat">
      <ChatHeader
        participiants={participiants}
        messages={messages.length}
        name="My chat"
        lastDate={messages.length ? new Date(messages[messages.length - 1].createdAt) : false} />
      <MessageList messages={messages} isLoaded={isLoaded} />
      <MessageInput isLoaded={isLoaded} />
    </div>
  );
}

export default Chat;
