import { useEffect, useRef, useState } from 'react';
import { getRelativeDateString } from '../helpers/dateHelper';
import EditModal from './modal/EditModal';
import Loader from './Loader';
import MessageGroup from './MessageGroup';

function MessageList({ messages, isLoaded }) {
  const ref = useRef(null);
  const [editedMessage, setEditedMessage] = useState({});
  const [showModal, setShowModal] = useState(false);
  const [messagesElems, setMessagesElems] = useState([]);

  const showEditModal = (message) => {
    setEditedMessage(message)
    setShowModal(true)
  }

  const closeEditModal = () => {
    setShowModal(false)
  }

  const mapMessagesGroups = (messagesMap) => {
    const groupsElems = [];
    const mapIterator = messagesMap.keys();
    let iterator = mapIterator.next();
    while (!iterator.done) {
      const messages = messagesMap.get(iterator.value);
      groupsElems.push(<MessageGroup
        showEditModal={showEditModal}
        day={iterator.value}
        messages={messages}
        key={iterator.value} />)
      iterator = mapIterator.next();
    }
    return groupsElems;
  }
  
  const divideMessagesByDay = (messages) => {
    const messagesMap = new Map();
    messages.forEach((message) => {
      const createdAt = new Date(message.createdAt);
      const key = getRelativeDateString(createdAt);
      if (messagesMap.has(key)) {
        const group = messagesMap.get(key);
        group.push(message);
        messagesMap.set(key, group)
      } else {
        messagesMap.set(key, [message]);
      }
    });
    return messagesMap;
  }

  useEffect(() => {
    setMessagesElems(mapMessagesGroups(divideMessagesByDay(messages)));
    //eslint-disable-next-line
  }, [messages])

  useEffect(() => {
    if (ref.current) {
      ref.current.scrollIntoView();
    }
  }, [messagesElems.length])

  return (
    <div className="chat__messages">
      { isLoaded ?
      (messagesElems.length > 0 ? messagesElems :
        <div>
          <p className="chat__empty" >This chat is empty</p>
          <p className="chat__empty" >You cannot send a message to an empty chat</p>
        </div>) :
      <Loader /> }
      <EditModal
        id={editedMessage.id}
        text={editedMessage.text}
        show={showModal}
        onClose={closeEditModal}
        title="Edit" />
      <div style={{ float:"left", clear: "both" }} ref={ref}></div>
    </div>
  );
}

export default MessageList;
