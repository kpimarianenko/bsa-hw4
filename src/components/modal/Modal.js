import '../../styles/Modal.css';

function Modal({ title, onClose = () => {}, children, show }) {
  return show ? (
    <div className="modal__layout">
      <div className="modal">
        <div className="modal__header">
            <h3 className="modal__title">{ title }</h3>
            <span onClick={onClose} className="modal__close">&times;</span>
        </div>
        <div className="modal__body">{ children }</div>
      </div>
    </div>
  ) : null;
}

export default Modal;
