import '../../styles/EditModal.css';
import { useContext, useEffect, useState } from 'react';
import Modal from './Modal';
import { Context } from '../../context';

function EditModal({ id, text, onClose, ...attrs }) {
  const { editMessage, deleteMessage } = useContext(Context);
  const [message, setMessage] = useState('');

  const onChangeHandler = (event) => {
    setMessage(event.target.value);
  }

  const submitMessageEdit = (event) => {
    event.preventDefault();
    if (message.length > 0) {
      editMessage(id, message);
      onClose();
    }
  }

  const onDeleteMessage = () => {
    deleteMessage(id);
    onClose();
  }

  useEffect(() => {
    if (text) {
      setMessage(text);
    }
  }, [text])

  return (
    <Modal onClose={onClose} {...attrs} >
      <form id="message-edit" onSubmit={submitMessageEdit}>
        <input value={message} onChange={onChangeHandler} placeholder="Write a message..." type="text" />
      </form>
      <div className="modal__controls">
        <button onClick={onClose} className="button button-cancel">Cancel</button>
        <i onClick={onDeleteMessage} className="far fa-trash-alt"></i>
        <input form="message-edit" type="submit" value="Edit" className="button button-confirm" />
      </div>
    </Modal>
  )
};

export default EditModal;