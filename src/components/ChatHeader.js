import { getRelativeDateString, getTimeFromDate } from '../helpers/dateHelper';

function ChatHeader({ name, participiants, messages, lastDate }) {
  return (
    <div className="chat__header">
      <div className="chat__info">
        <h3>{ name }</h3>
        <p>{`${participiants} participiants`}</p>
        <p>{`${messages} messages`}</p>
      </div>
    <p>{lastDate ? 
      `Last message ${getRelativeDateString(lastDate)} at ${getTimeFromDate(lastDate)}` :
      'Chat is empty' }
    </p>
    </div>
  );
}

export default ChatHeader;
