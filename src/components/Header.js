import '../styles/Header.css';
import './Logo';
import Logo from './Logo';
import Profile from './Profile';
import LogoImg from '../assets/images/logo.png';

function Header({ user }) {
  return (
    <div className="header">
      <Logo appName="DogeGram" appLogo={LogoImg} />
      { user ? <Profile name={user.name} avatar={user.avatar} /> : null}
    </div>
  );
}

export default Header;
