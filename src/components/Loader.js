import '../styles/Loader.css';

function Loader() {
  return (
    <div className="loader__layout">
      <div className="loader"></div>
    </div>
  );
}

export default Loader;