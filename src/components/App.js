import '../styles/App.css';
import Header from './Header';
import Chat from './Chat';
import Footer from './Footer';
import uuidv4 from '../helpers/uuidv4';
import { Context } from '../context'; 
import { useEffect, useState } from 'react';
import { getRandomInt } from '../helpers/randomHelper';
import { getMessages } from '../helpers/apiHelper';

function App() {
  const [messages, setMessages] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [participiants, setParticipiants] = useState(0);
  const [curUser, setCurUser] = useState();

  const getUsersListFromMessages = messages => {
    const users = [];
    messages.forEach(({ userId, avatar, user}) => {
      if (!arrayHasUser(users, userId)) {
        users.push({
          id: userId,
          name: user,
          avatar: avatar,
        });
      }
    });
    return users;
  }

  const arrayHasUser = (users, id) => {
    let found = false;
    for(var i = 0; i < users.length; i++) {
      if (users[i].id === id) {
        found = true;
        break;
      }
    }
    return found;
  }

  const getCurUser = () => {
    return curUser;
  }

  const toggleLike = (messageId) => {
    const msgs = [...messages];
    setMessages(msgs.map(el => {
      if (el.id === messageId) {
        el.isLiked = !el.isLiked;
      }
      return el;
    }));
  }

  const addMessage = (messageText) => {
    const message = {
      id: uuidv4(),
      userId: curUser.id,
      user: curUser.name,
      avatar: curUser.avatar,
      text: messageText,
      createdAt: (new Date()).toISOString(),
      editedAt: ''
    };
    setMessages([...messages, message]);
  }

  const deleteMessage = (messageId) => {
    const msgs = [...messages];
    setMessages(msgs.filter(message => message.id !== messageId));
  }

  const editMessage = (messageId, text) => {
    const msgs = [...messages];
    setMessages(msgs.map(el => {
      if (el.id === messageId) {
        el.text = text;
        el.editedAt = (new Date()).toISOString();
      }
      return el;
    }));
  }

  useEffect(() => {
    getMessages()
    .then(messages => {
      setMessages(messages);
      setIsLoaded(true);
      return messages;
    })
    .then(messages => {
      const users = getUsersListFromMessages(messages);
      setParticipiants(users.length);
      if (users.length > 0) {
        setCurUser(users[getRandomInt(0, users.length - 1)]);
      }
    });
    // eslint-disable-next-line
  }, [])

  return (
    <Context.Provider value={{
      getCurUser, addMessage, toggleLike, deleteMessage, editMessage
    }}>
      <div className="app">
        <Header user={curUser} />
        <Chat participiants={participiants} messages={messages} isLoaded={isLoaded} />
        <Footer />
      </div>
    </Context.Provider>
  );
}

export default App;
