import { useContext, useState } from "react";
import { Context } from '../context';

function MessageInput({ isLoaded }) {
  const { addMessage, getCurUser } = useContext(Context);
  const [message, setMessage] = useState('');

  const sendMessage = (event) => {
    event.preventDefault();
    console.log(isLoaded)
    if (message.length > 0 && getCurUser() && isLoaded) {
      addMessage(message);
      setMessage('');
    }
  }

  const onChangeHandler = (event) => {
    setMessage(event.target.value);
  }

  return (
    <div className="chat__input">
      <form id="message-input" onSubmit={sendMessage}>
        <input value={message} onChange={onChangeHandler} placeholder="Write a message..." type="text" />
      </form>
      <input form="message-input" type="submit" value="Send" className="button button-confirm" />
    </div>
  );
}

export default MessageInput;
