import '../styles/Footer.css';

function Footer() {
  return (
    <div className="footer">
      &copy; Powered by Binary Studio Winter Academy student Marianenko Roman 
    </div>
  );
}

export default Footer;
