import { getTimeFromDate } from '../helpers/dateHelper';
import { useContext, useEffect, useState } from 'react';
import { Context } from '../context';

function Message({ message, showEditModal }) {
  const { getCurUser, toggleLike } = useContext(Context);
  const [isCurUserMessage, setIsCurUserMessage] = useState(false);
  const { id, userId, user: name, text, avatar: avaUrl, createdAt, isLiked } = message;

  const onEditClick = () => {
    showEditModal(message);
  }

  const toggleLikeFunc = () => {
    toggleLike(id)
  }

  useEffect(() => {
    const curUser = getCurUser();
    setIsCurUserMessage(curUser && curUser.id === userId)
  }, [getCurUser, userId])

  return (
    <div className="chat__message-wrapper">
      <div className={`chat__message ${isCurUserMessage ? 'message__yours' : ''}`}>
          { isCurUserMessage ? null : <img src={avaUrl} alt={`${name}'s ava`} />}
          <div className="message__body">
              <h4 className="message__username">{isCurUserMessage ? 'You' : name}</h4>
              <p className="message__text">{text}</p>
          </div>
          <div className="message__aside">
            <div className="message__interact">
                {!isCurUserMessage ?
                <i onClick={toggleLikeFunc} className={`${isLiked ? 'fas' : 'far'} fa-heart`}></i> :
                <i onClick={onEditClick} className="far fa-edit"></i>}
            </div>
            <p className="message__time">{getTimeFromDate(createdAt)}</p>
          </div>
      </div>
    </div>
  );
}

export default Message;